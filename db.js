var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	console.log('connedted');
});

var vehicleSchema = mongoose.Schema({
    id: { type: Number, unique: true, required: true,  autoIncrement: true},
    brand: { type: String, minlength: 1,  required: true},
    name: { type: String, required: true, minlength: 1},
    year: { type: Number},
    type: { type: String},
    engine_type: { type: String},
    rating: { type: Number, max: 10}
});

var Vehicle = mongoose.model('Vehicle', vehicleSchema);

var vehicleModels = {
    save: function(callback){
		var vehicle = new Vehicle({
			id: 4,
	    	brand: 'Toyota',
	    	name: 'Altis',
	    	year: 2016,
	    	type: 'Car',
	    	engine_type: 'Gas',
	    	rating: 8,
	  	});

		vehicle.save(function(err, res) {
	  		if (err) 
	  			callback(err)
	  		else
	  			callback(res);

	  		console.log('Vehicle saved successfully!');
		});
	},
    all: function(callback){
    	Vehicle.find(function (err, results) {
    		callback(results);
  		});
    },

    update: function(callback){
		Vehicle.findOne({ id: 1 }, function (err, res){
  			res.name = 'jason borne';
  			res.save(function(err, res){
  				if (err) 
	  				callback(err);
	  			else
	  				callback(res);

  				console.log('Vehicle saved successfully!');
  			});
		});    	
    },

    getByID: function(vehicleID, callback){
    	Vehicle.find({id : vehicleID}, function(err, res){
		  	callback(res);
		});
    },

    getByType: function(vehicleType, callback){
    	Vehicle.find({type : vehicleType}, function(err, res){
		  	callback(res);
		});
    }
}

module.exports = vehicleModels;