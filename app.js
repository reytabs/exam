var express = require('express')
var vehicle = require('./db.js');
var app = express()

app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.get('/vehicle/create', function (req, res) {  	
 	vehicle.save(function(result){
 		res.send(result);
 	});
})

app.get('/vehicle/all', function (req, res){
	vehicle.all(function(result){
		res.send(result);
	});
})

app.get('/vehicle/update', function (req, res){
	vehicle.update(function(result){
		res.send(result);
	});
})

app.get('/vehicle/:vehicleID', function (req, res){
	var vehicleID = req.params.vehicleID;
	vehicle.getByID(vehicleID, function(result){
		res.send(result);
	});
})

app.get('/vehicle/:vehicleType', function (req, res){
	var vehicleType = req.params.vehicleType;
	vehicle.getByType(vehicleType, function(result){
		res.send(result);
	});
})

app.listen(3000, function () {
  console.log('Kevin Rey APP on port 3000!')
})